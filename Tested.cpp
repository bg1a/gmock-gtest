#include <iostream>
#include <stdexcept>
#include <memory>
#include "Tested.h"

void Tested::PenUp(void)
{
    std::cout << "In tested PenUp\n";
    throw std::runtime_error(__func__);
}
void Tested::GoTo(int x, int y)
{
    std::cout << "In GoTo, args are: \n";
    std::cout << "x: " << x << ", " << "y: " << y << std::endl;
}
int Tested::getX(bool force)
{
    if(force)
    {
        return 27;
    }
    return 0;
}
int Tested::getY(bool force)
{
    std::cout << "---> In " << __func__ << std::endl;
    if(force)
    {
        return 131;
    }
    return 0;
}

std::shared_ptr<ReturnValue> Tested::testR(void)
{
    std::cout << "returning ReturnValue\n";  
    return std::make_shared<ReturnValue>();
}

