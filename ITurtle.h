
#pragma once
#include <memory>

class ReturnValue
{
private:
    int a;
    char b;
public:
    int getA(void){return a;}
    int getB(void){return b;}
};

class ITurtle
{
public:
    virtual ~ITurtle(){}
    virtual void PenUp(void) = 0;
    virtual void GoTo(int x, int y) = 0;
    virtual int getX(bool force) = 0;
    virtual int getY(bool force) = 0;
    virtual std::shared_ptr<ReturnValue> testR(void) = 0;

    virtual int getTwo(void) = 0;
};



