#include "gtest/gtest.h"
#include "gmock/gmock.h"

using ::testing::_;
using ::testing::Invoke;
using ::testing::Return;

struct Res{
    int a;
    double b;
};

// interface for mocking
class IBar
{
public:
    virtual ~IBar(void)
    {}
    virtual Res foo(float arg) = 0;
    virtual Res baz(int a, char b) = 0;

};

class MockIBar : public IBar
{
public:
    MockIBar()
    {}
    virtual ~MockIBar()
    {}
    MOCK_METHOD1(foo, Res(float arg));
    MOCK_METHOD2(baz, Res(int a, char b));
};

void checkBar(IBar *ib, float arg)
{
    Res res = ib->foo(arg);
    std::cout << " ---> In foo(), res is: " << res.a << std::endl;
}

TEST(CheckSideEffect, CheckSideFoo)
{
    MockIBar mbar;
    Res tmp_res = {
        .a = 33,
        .b = 88.8,
    };

    EXPECT_CALL(mbar, foo(_))
        .WillOnce(Invoke(
                    [&tmp_res](int arg) mutable -> decltype(tmp_res)
                    {
                        tmp_res.a = 78;
                        tmp_res.b = 5.9;

                        tmp_res.a += arg;
                        return tmp_res;
                    }
                    ));


    checkBar(&mbar, 17);
    std::cout << " ---> In test, tmp_res.a is: " << tmp_res.a << std::endl;
}



