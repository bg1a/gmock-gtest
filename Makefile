TARGET := main
CXX := g++
PWD := $(shell pwd)
INC_DIR := -I$(PWD)
SRC_DIR := $(PWD)
SRC := $(wildcard $(PWD)/*.cpp)
HDR := $(wildcard $(PWD)/*.h)
GMOCK_DIR := ../../../devel/gmock/gmock-1.7.0
SRC := $(wildcard $(SRC_DIR)/*.cpp)

CXXFLAGS += -std=c++11 \
			-Wall \
			-DGTEST_LANG_CXX11=1 \
			-DGTEST_HAS_TR1_TUPLE=1 \
			-DGTEST_USE_OWN_TR1_TUPLE=1 

INC_DIR += -I$(GMOCK_DIR)/include/ -I$(GMOCK_DIR)/gtest/include/
LDLIBS := $(GMOCK_DIR)/libgmock.a $(GMOCK_DIR)/libgmock_main.a \
	$(GMOCK_DIR)/gtest/libgtest_main.a $(GMOCK_DIR)/gtest/libgtest.a -pthread

all: $(SRC) $(HDR)
	$(CXX) $(CXXFLAGS) -o $(TARGET) $(SRC) $(CXXFLAGS) $(INC_DIR) $(LDLIBS)

run:
	@echo --- Run all tests ---
	@-$(PWD)/$(TARGET) ; if [ $$? -eq 127 ]; \
		then echo "Try to invoke \"make\" and then \"make run\""; fi
	@echo --- Stop all tests ---

preprocess:
	@echo $(PREPROCESS_TARGET)
	$(CXX) $(CXXFLAGS) $(INC_DIR) -E $(PREPROCESS_TARGET)

fake:
	@eval  "echo $(MAKECMDGOALS)"

clean:
	@rm -fv $(TARGET)
		   
