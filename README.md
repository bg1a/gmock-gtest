# Gmock/Gtest Sample #


## HowTo ##

1. Download Gmock from: https://code.google.com/p/googlemock/downloads/list
2. Install CMake:
        sudo apt-get install cmake
3. In Gmock folder run:
       cmake CMakeLists.txt
4. Finally, execute *make*. After this step, there will be all necessary libs in 
        Gmock directory.
5. Edit *Makefile* in the tests directory. Set proper path to the Gmock folder 
        (in $(GMOCK_DIR) variable).
6. Run *make*.
7. To fullfil the tests, run *main* in tests directory.

**TODO** Add gmock repository to the project.

## Usefull links ##
http://www.thebigblob.com/getting-started-with-google-test-on-ubuntu

https://code.google.com/p/googlemock/wiki/ForDummies

https://code.google.com/p/googlemock/wiki/CookBook

https://code.google.com/p/googletest/wiki/AdvancedGuide