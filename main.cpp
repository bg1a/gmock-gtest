
#include <iostream>
#include <stdexcept>
#include <memory>

#include "Tested.h"
#include "MockTested.h"

#include "gtest/gtest.h"
using ::testing::_;
using ::testing::Return;
using ::testing::Invoke;

void runTurtle(ITurtle *it)
{
    it->GoTo(10, 15);
}

void stopDrawing(ITurtle *it)
{
    it->PenUp();
}

void returnValueTwo(ITurtle *it)
{
    std::cout << "---> expect 2 as a return value: " << it->getTwo() << std::endl;
}


void printPosition(ITurtle *it)
{
    std::cout << "-- x is: " << it->getX(true) << " ";
    std::cout << "-- y is: " << it->getY(true) << std::endl;

    if(it->getX(true) > 50)
    {
        throw std::runtime_error("smth bad in printPosition");
    }
    if(it->getY(true) > 100)
    {
        throw std::runtime_error("####### in printPosition");
    }
}

void getReturn(ITurtle *it)
{
    std::shared_ptr<ReturnValue> rv(it->testR());
    if(rv == NULL)
    {
        throw std::runtime_error("Bad return!"); 
    }
}

TEST(ReturnValueTest, GetReturn)
{
    //MockTested mt;
    Derived d;
    EXPECT_CALL(d, testR())
        //.WillOnce(Return(std::shared_ptr<ReturnValue>(0)));
        .WillOnce(Return(std::make_shared<ReturnValue>()));
    EXPECT_ANY_THROW(getReturn(&d));
}

TEST(PainterTest, PenIsUp)
{
    MockTested mt;
    EXPECT_CALL(mt, PenUp());
    stopDrawing(&mt);
}

TEST(MoveTest, IsGoTo)
{
    MockTested mt;
    EXPECT_CALL(mt, GoTo(10, 15));
    runTurtle(&mt);
}

TEST(ExceptionTest, IsThrow)
{
    MockTested mt;
    EXPECT_NO_THROW(mt.PenUp());
}

TEST(PositionTest, ShowCoords)
{
    MockTested mt;
    Tested tt;
    EXPECT_CALL(mt, getX(true))
        .Times(2)
        .WillRepeatedly(Return(89));
    
    EXPECT_CALL(mt, getY(true))
        .WillRepeatedly(Invoke(&tt, &Tested::getY));
    
    EXPECT_NO_THROW(printPosition(&mt));

    //printPosition(&mt);
}

TEST(returnValueTest, isReturnTwo)
{
    MockTested mt;
    EXPECT_CALL(mt, getTwo())
        .WillOnce(Return(3));
    returnValueTwo(&mt);
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();

    return 0;
}


