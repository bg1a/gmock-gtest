
#pragma once
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <memory>
#include "ITurtle.h"

using ::testing::Return;

class MockTested : public ITurtle 
{
public:
    MockTested(){
        ON_CALL(*this, getTwo())
            .WillByDefault(Return(2));
    }
    MOCK_METHOD0(PenUp, void(void));
    MOCK_METHOD2(GoTo, void(int x, int y));
    MOCK_METHOD1(getX, int(bool force));
    MOCK_METHOD1(getY, int(bool force));
    MOCK_METHOD0(testR, std::shared_ptr<ReturnValue>(void));

    // Function is going to return 2 always
    MOCK_METHOD0(getTwo, int(void));
};

class Derived : public MockTested
{
public:
    Derived()
    {
        std::cout << "In derived\n";
    }
    int a;
};
