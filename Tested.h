

#pragma once

#include <memory>
#include "ITurtle.h"

class Tested : public ITurtle
{
public:
    void PenUp(void);
    void GoTo(int x, int y);
    int getX(bool force);
    int getY(bool force);
    std::shared_ptr<ReturnValue> testR(void);

    int getTwo(void)
    {
        return 2;
    }
};

