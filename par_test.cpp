#include <array>
#include "gtest/gtest.h"

template<class T>
class ArrayTest : public ::testing::Test{
public:
    virtual void SetUp(void)
    {
        std::cout << "---> In SetUp\n";
    }
    virtual void TearDown(void)
    {
        std::cout << "<--- In TearDown\n";
    }
    
    static const int mFaultyIdx = -1;
    static const size_t mArrSize = 10;
    std::array<T, mArrSize> mArray;

};


TYPED_TEST_CASE_P(ArrayTest);

TYPED_TEST_P(ArrayTest, ThrowIfIdxHasNegativeValue)
{
    // Underlying array type should support at() and throw in case of
    // faulty index
    EXPECT_ANY_THROW( this->mArray.at(this->mFaultyIdx) ); 
}

REGISTER_TYPED_TEST_CASE_P(ArrayTest, ThrowIfIdxHasNegativeValue);

typedef ::testing::Types<char, int> ArrayTypes;

INSTANTIATE_TYPED_TEST_CASE_P(Test, ArrayTest, ArrayTypes);



